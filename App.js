const express = require('express');
const connectdatabase = require('./database/Database');
var cors = require('cors');
const beers = require('./api/Beers');
const app = express();

app.get('/', (req, res) => res.send('BeerLogBE'));

connectdatabase();

// cors
app.use(cors({ origin: true, credentials: true }));;

// Init Middleware
app.use(express.json({ extended: false }));

app.use('/beers', beers);

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));