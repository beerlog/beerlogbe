const mongoose = require('mongoose');
const config = require('config');
const databasemongo = config.get('mongoURI');

const connectDB = async () => {
  try {
    await mongoose.connect(
      databasemongo,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );

    console.log('MongoDB is Connected...');
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
};

module.exports = connectDB;