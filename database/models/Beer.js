const mongoose = require('mongoose');

const BeerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  brewer: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  tasted_date: {
    type: Date
  },
  notes: {
    type: String
  },
  updated_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Beer = mongoose.model('beer', BeerSchema);